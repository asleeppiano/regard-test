const ComponentC = {
	template: `<div class="c">
			<h2 class="title">componentC</h2>
			<div class="wrapper">
				<slot name="test" prop="prop" />
			</div>
		</div>`
}

const ComponentB = {
	components: {
		ComponentC
	},
	template: `<div class="b">
		<h2 class="title">componentB</h2>
		<div class="wrapper">
			<component-c>
				<template v-for="(_, name) in $slots" v-slot:[name]="slotData">
					<slot :name="name" v-bind="slotData" />
				</template>
			</component-c>
		</div>
	</div>`
}

export default {
	components: {
		ComponentB
	},
	template: `<div class="a">
					<h2 class="title">componentA</h2>
					<div class="wrapper">
						<component-b>
							<template v-for="(_, name) in $slots" v-slot:[name]="slotData">
								<slot :name="name" v-bind="slotData" />
							</template>
						</component-b>
					</div>
				</div>`,
};


