# Test task

Первое и третье задания лежат в папке `node`

Второе в `browser`

## Запуск

```sh
git clone git@gitlab.com:asleeppiano/regard-test.git
cd regard-test
```

### Node (1, 3 task)

```sh
cd node
npm i
npm run test
```

### Browser (2 task)

```sh
cd browser
npm i
npm run serve
```
