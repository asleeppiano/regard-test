function isBool(value) {
	return value === true || value === false;
}

export function booleanToInt(item) {
	if (typeof item !== 'object') {
		if (isBool(item)) {
			return Number(item);
		}
	}
	for (let [k, v] of Object.entries(item)) {
		if (isBool(v)) {
			item[k] = Number(v)
		}
		if (typeof v === 'object') {
			item[k] = booleanToInt(v)
		}
	}
	return item;
}
