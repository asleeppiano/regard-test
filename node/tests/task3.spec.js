import { test } from "uvu";
import * as assert from "uvu/assert";
import { booleanToInt } from "../task3.js";

test("it should work", () => {
	let result = booleanToInt('qwerty')
	assert.equal(result, 'qwerty');

	result = booleanToInt(1)
	assert.equal(result, 1)

	result = booleanToInt(false)
	assert.equal(result, 0);

	result = booleanToInt(true)
	assert.equal(result, 1);

	result = booleanToInt([1, 'qwerty', false])
	assert.equal(result, [1, 'qwerty', 0]);

	result = booleanToInt([1, 'qwerty', { a: true }])
	assert.equal(result, [1, 'qwerty', { a: 1 }]);

	result = booleanToInt({ a: { b: true }, c: false, d: 'qwerty' })
	assert.equal(result, { a: { b: 1 }, c: 0, d: 'qwerty' });

	result = booleanToInt({
		date1: {
			date1_1: 1,
			date1_2: [
				{
					date2_1: false,
					date2_2: "str1",
				},
				{
					date2_3: true,
					date2_4: "str2",
				},
				{
					date2_5: false,
					date2_6: "str1",
				},
			],
			date1_3: false,
			date1_4: {
				date3_1: true,
				date3_2: false,
				date3_3: "str1",
				date3_4: true,
				date3_4: 123,
			},
			date1_5: "true",
		},
	});
	assert.equal(result, {
		date1: {
			date1_1: 1,
			date1_2: [
				{
					date2_1: 0,
					date2_2: "str1",
				},
				{
					date2_3: 1,
					date2_4: "str2",
				},
				{
					date2_5: 0,
					date2_6: "str1",
				},
			],
			date1_3: 0,
			date1_4: {
				date3_1: 1,
				date3_2: 0,
				date3_3: "str1",
				date3_4: 1,
				date3_4: 123,
			},
			date1_5: "true",
		},
	});
});

test.run();
