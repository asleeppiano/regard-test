import { test } from 'uvu';
import * as assert from 'uvu/assert';
import { copy } from '../task1.js';

test('it should work', () => {
	const obj = {
		a: {
			b: {
				c: 1,
			},
		},
		d: {
			e: 3
		},
		f: {
			g: 4,
		},
	};

	const result = copy(obj, ["a.b.c", "f.g"]);

	result.a.b.c = 3;
	result.d.e = 5;
	result.f.g = 6;

	assert.equal(result, {
		a: {
			b: {
				c: 3
			}
		},
		d: {
			e: 5
		},
		f: {
			g: 6
		}
	});
	assert.equal(obj, {
		a: {
			b: {
				c: 1
			}
		},
		d: {
			e: 5
		},
		f: {
			g: 4
		}
	});
});

test('it should copy keys if path is right', () => {
	const obj = {
		a: {
			b: {
				c: 1,
			},
		},
		d: {
			e: 3
		},
		f: {
			g: 4,
		},
	};

	const result = copy(obj, ["a.b", "f.g"]);

	result.a.b.c = 3;
	result.d.e = 5;
	result.f.g = 6;

	assert.equal(result, {
		a: {
			b: {
				c: 3
			}
		},
		d: {
			e: 5
		},
		f: {
			g: 6
		}
	});
	assert.equal(obj, {
		a: {
			b: {
				c: 3
			}
		},
		d: {
			e: 5
		},
		f: {
			g: 4
		}
	});
});

test("it shouldn't copy if it exist but path is wrong", () => {
	const obj = {
		a: {
			b: {
				c: 1,
			},
		},
		d: {
			e: 3
		},
		f: {
			g: 4,
		},
	};

	const result = copy(obj, ["b", "f"]);

	result.a.b.c = 3;
	result.d.e = 5;
	result.f.g = 6;

	assert.equal(result, {
		a: {
			b: {
				c: 3
			}
		},
		d: {
			e: 5
		},
		f: {
			g: 6
		}
	});
	assert.equal(obj, {
		a: {
			b: {
				c: 3
			}
		},
		d: {
			e: 5
		},
		f: {
			g: 6
		}
	});
});

test("it shouldn't copy an object if we cannot find provided key", () => {
	const obj = {
		a: {
			b: {
				c: 1,
			},
		},
		d: {
			e: 3
		},
		f: {
			g: 4,
		},
	};

	const result = copy(obj, ["d.e.d"]);

	result.a.b.c = 3;
	result.d.e = 5;
	result.f.g = 6;

	assert.equal(result, {
		a: {
			b: {
				c: 3
			}
		},
		d: {
			e: 5
		},
		f: {
			g: 6
		}
	});
	assert.equal(obj, {
		a: {
			b: {
				c: 3
			}
		},
		d: {
			e: 5
		},
		f: {
			g: 6
		}
	});
});

test("it shouldn't fail if an object has null value", () => {
	const obj = {
		a: {
			b: null
		},
		c: {
			d: [1, 2],
		},
	};

	const result = copy(obj, ["a.b.c"]);

	result.a.b = { c: 3 };
	result.c.d[0] = 4;

	assert.equal(result, {
		a: {
			b: {
				c: 3
			}
		},
		c: {
			d: [4, 2]
		}
	});
	assert.equal(obj, {
		a: {
			b: {
				c: 3
			}
		},
		c: {
			d: [4, 2]
		}
	});
});

test("it should do array shallow copy if key value is array", () => {
	const obj = {
		a: {
			b: null
		},
		c: {
			d: [1, 2],
		},
	};

	const result = copy(obj, ["c.d"]);

	result.c.d[0] = 4;

	assert.equal(result, {
		a: {
			b: null
		},
		c: {
			d: [4, 2]
		}
	});
	assert.equal(obj, {
		a: {
			b: null
		},
		c: {
			d: [1, 2]
		}
	});
});

test.run();
