export function copy(obj, keyList) {
	let result = {};
	keyList.forEach((keys) => {
		let splittedKeys = keys.split(".");
		if (!hasKeys(splittedKeys, obj)) {
			return;
		}
		const keysObj = convertArrayOfKeysToNestedObject(splittedKeys);
		result = { ...result, ...keysObj };
	});
	result = deepCopy(result, obj);
	for (let k of Object.keys(obj)) {
		if (!result.hasOwnProperty(k)) {
			result[k] = obj[k];
		}
	}
	return result;
}

function hasKeys(keys = [], obj = {}) {
	for (let key of keys) {
		if (obj && obj.hasOwnProperty(key)) {
			obj = obj[key];
		} else {
			return false
		}
	}
	return true;
}

function deepCopy(obj1 = {}, obj2 = {}) {
	let result = {};
	for (let [k, v] of Object.entries(obj2)) {
		if (obj1 && obj1.hasOwnProperty(k)) {
			if (
				typeof v === "string" ||
				typeof v === "number" ||
				typeof v === "undefined" ||
				typeof v === "function"
			) {
			} else if (obj1[k]) {
				result[k] = deepCopy(obj1[k], obj2[k]);
			} else {
				if (Array.isArray(v)) {
					result[k] = v.slice();
				} else {
					result[k] = v;
				}
			}
		}
	}
	return result;
}

function convertArrayOfKeysToNestedObject(keys = [], initObject = null) {
	if (keys) {
		return keys.reduceRight((all, item) => {
			return { [item]: all };
		}, initObject);
	}
	return {}
}
